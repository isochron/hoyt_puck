#ifndef MBED_PUCK_STATE_H
#define MBED_PUCK_STATE_H

/**
 * Header for PuckState
 *
 * Should be easy enough to grok, will add docs when it gets more complicated.
 *
 * April 2022, cc
 */ 

#include "stdint.h"
#include "mbed.h"

extern InterruptIn power_btn;
extern InterruptIn m_btn;
extern InterruptIn r_btn;
extern InterruptIn up_btn;
extern InterruptIn down_btn;
extern AnalogIn throttle_adc;

class PuckState {
    public:
        bool power;
        bool m;
        bool r;
        bool up;
        bool down;
        unsigned short throttle;
        float throttle_voltage;
        float throttle_percent;

        PuckState(void);
        void print(void);
        void update(void);
};

#endif