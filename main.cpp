#include "EventQueue.h"
#include "PinNamesTypes.h"
#include "ThisThread.h"
#include "cmsis_os2.h"
#include "mbed.h"
#include "stdint.h"
#include <cstdint>
#include "PuckState.h"

InterruptIn power_btn(SW_1, PullDown);
InterruptIn m_btn(SW_2);
InterruptIn r_btn(SW_3);
InterruptIn up_btn(SW_4);
InterruptIn down_btn(SW_5);
AnalogIn throttle_adc(THROTTLE);

DigitalOut led_g(LED_R);

PuckState state;                // The current shared state of the MCU's periphs
Mutex state_mutex;              // A mutex to use when writing to the global state
EventQueue queue;               // Event queue to handle interrupts safely

/**
 * Direction Change
 *
 * An example of doing stuff with a real button change handled safely by the OS with a queue
 */
void direction_change() {
    printf("Changed direction, now: U=%d | D=%d\n", state.up, state.down);
}

/**
 * Direction IRQ
 *
 * An example of handling a button request, tossing the function call into the event queue.
 */
void direction_irq() {
    queue.call(&direction_change);
}

/**
 * Update global current hardware state.
 *
 * Initiates hardware reads when the mutex is available.
 */
void update_state() {
    state_mutex.lock();
    state.update();
    state_mutex.unlock();
}

int main()
{
    Thread eventThread;
    eventThread.start(callback(&queue, &EventQueue::dispatch_forever));

    up_btn.rise(&direction_irq);
    down_btn.rise(&direction_irq);

    printf("Init ok\n");

    while (true) {
        update_state();
        printf("%f\n", state.throttle_percent);
        ThisThread::sleep_for(500ms);
    }
    
}
