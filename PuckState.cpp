/**
 * Implementation for PuckState
 *
 * This is a class that contains state information about the puck's hardware.
 * I used a class instead of a simpler struct here because I figure we're going
 * to want to be able to add more complicated methods as we go along.
 *
 * April 2022, cc
 */

#include "PuckState.h"

/**
 * Default constructor
 *
 * Initializes the state by doing a normal update.
 */
PuckState::PuckState(void) 
{
    update();
}

/**
 * Update hardware state
 *
 * Does GPIO and ADC reads as needed to update the object's values.
 */
void PuckState::update()
{
    power = power_btn.read();
    m = m_btn.read();
    r = r_btn.read();
    up = up_btn.read();
    down = down_btn.read();
    throttle = throttle_adc.read_u16();
    throttle_voltage = throttle_adc.read_voltage();
    throttle_percent = throttle_adc.read();
}

/**
 * Prints values for debug purposes
 *
 * This is annoying looking in the console.
 */
void PuckState::print()
{
    printf("Power button: %d\n", power);
    printf("M button: %d\n", m);
    printf("R button: %d\n", r);
    printf("Up button: %d\n", up);
    printf("Down button: %d\n", down);
    printf("Throttle: %#04x\n", throttle);
    printf("Throttle percent: %f\n", throttle_percent);
}
