# Pin Names

## Introduction
The target Real Actual Puck™ is set up as a custom Mbed target.

The peripheral pins are set into macros to use.

For example, instead of initializing a DigitalOut like this:
```
DigitalOut led_r(PA_6);
```
you can now initialize it like this:
```
DigitalOut led_r(LED_R);
```

The pin names are entered as best as I could tell, and I think they're all there, though some that we don't know what exactly they do yet have generic names.

If you find better names for pins, feel free to update `targets/TARGET_PUCK/PinNames.h` where they're defined.

## Macro Values to Pin Table
These are easy enough to read in the code, but for your convenience:

| Macro | Pin |
| ----- | --- |
| LED_R | PA_4 |
| LED_G | PA_5 |
| LED_B | PA_6 |
| SW_1  | PC_14 |
| SW_2  | PB_6 |
| SW_3  | PB_8 |
| SW_4  | PB_9 |
| SW_5  | PC_13 |
| THROTTLE | PA_7 |
| RADIO_DIO | PB_15 |
| RADIO_SCK | PB_13 |
| RADIO_NSS | PB_12 |
| RADIO_IO_1 | PB_14 |
| RADIO_IO_2 | PA_8 |
| RADIO_IO_3 | PA_10 |
| RADIO_IO_4 | PA_9 |
| BUZZER | PA_15 |
| VIBRATOR | PA_3 |
| BIND | PB_2 |
| USB_POS | PA_11 | 
| USB_NEG | PA_12 |
| NCHARGE | PB_5 |
| BAT_MON | PB_0 |
| VBUS_ADC | PB_1 |
| U3_SCL | PB_10 |
| U3_SDA | PB_11 |
| CHARGE_OUT | PB_7 |