/**
* @file PpmManager.hpp
* @brief Manages the PPM dataframe and transmission between board and puck.
*/

#ifndef PPM_MANAGER_HPP
#define PPM_MANAGER_HPP

#include <stdint.h>

namespace isochron

{

struct ppmFrame 
{
    uint8_t firstByte {0x58}; //TODO: What's this?
    uint32_t channel1 {0x00000000}; //TODO: Throttle channel?
    uint32_t channel2 {0x00000000};
    uint32_t channel3 {0xDC05DC05};
    uint32_t channel4 {0xDC05DC05};
    uint32_t channel5 {0xDC05DC05};
    uint32_t channel6 {0xDC05DC05};
    uint32_t channel7 {0xDC05DC05};
    uint32_t channel8 {0xDC05DC05};
    uint32_t channel9 {0xDC05DC05}; //This value increments by 0x0010_0000 for every frame.
    uint8_t padByte{0x00};
};


class ppmManager {
};


} //namespace isochron

#endif //ifndef