
/**
@file: spi.hpp

Software SPI library

@brief: Does not support inverse polarity or phase.
Byte size is hard-coded to sizeof(uint8_t)
*/

#ifndef SW_SPI_DEVICE_HPP
#define SW_SPI_DEVICE_HPP



#include "DigitalIn.h"
#include "DigitalOut.h"
#include <cstdint>
#include <cstddef>


namespace isochron
{

/**
This struct defines the SW SPI bus pins.
*/
struct swSpiBus
{
    mbed::DigitalIn miso;
    mbed::DigitalOut mosi;
    mbed::DigitalOut sclk;
};


/**
This class represents a single device on the SW SPI bus.

*/
class swSpiDevice
{
public:

    swSpiDevice(swSpiBus& bus, uint8_t deviceAddress);

    bool readFrom(uint8_t address, uint8_t* readBuf, size_t numBytes);

    void setFrequency(float freqHz);

    bool writeTo(uint8_t address, uint8_t* writeBuf, size_t numBytes);

protected:

    uint8_t m_address; //Device address on the bus
    swSpiBus m_bus; //the sw spi bus the device is on
    float m_freqHz; //the device spi frequency in Hz.
    float m_clkDelay; //reciprocal of m_freqHz over 2. Save as member variable every time we change frequency.

    uint8_t transferByte(uint8_t byte);

};

} // namespace isochron

#endif