
/**
@file: spi.cpp

Software SPI library
*/


#include <cstdint>

#include "ThisThread.h"
#include "swSpiDevice.hpp"

namespace isochron
{

swSpiDevice::swSpiDevice(swSpiBus& bus, uint8_t deviceAddress): m_bus{bus}, m_address{deviceAddress}
{
    ;
}

/**
Read one or multiple bytes from the SPI device
@param: address - register address to read from.
@param: readBuf - buffer to read into
@param: numBytes - number of bytes to read
@note: The user is responsible for making sure the buffer is big enough to hold the specified number of bytes.
*/
bool swSpiDevice::readFrom(uint8_t address, uint8_t* readBuf, size_t numBytes)
{
    transferByte(m_address);
    for (int i=0; i < numBytes; i++)
    {
        readBuf[i] = transferByte(0x00); // Transferring 0s will leave MOSI low while reading MISO.
    }
    return true;
}

/**
Set the SCL frequency for the device
@param: freqHz - the frequency, in hertz, to set the device SCL to.
*/
void swSpiDevice::setFrequency(float freqHz)
{
    m_freqHz = freqHz;
    m_clkDelay = 1.0/m_freqHz/2;
}

uint8_t swSpiDevice::transferByte(uint8_t byte)
{
    uint8_t read = 0;
    for (int bit = 0; bit < sizeof(uint8_t); bit++)
    {
        m_bus.mosi.write(((byte >> bit) & 0x01) != 0);
 
        /*
        if (phase == 0)
        {
            if (miso->read())
                read |= (1 << bit);
        }
        */
 
        m_bus.sclk.write(true);
 
        rtos::ThisThread::sleep_for(1000/m_freqHz/2);
 
        /*
        TODO: This should read into a buffer?
        if (m_bus.miso.read())
        {
            read |= (1 << bit);
        }
        */
 
        m_bus.sclk.write(false);
 
        rtos::ThisThread::sleep_for(1000/m_freqHz/2);
    }
    
    return read;
}

/**
Write a value or values to the specified register address.
@param: address - register address to write to on the device
@param: writeBuf - array of bytes to write to the specified register
@param: numBytes - number of bytes to write
@note: User is responsible for ensuring writeBuf is big enough for the specified number of bytes.
*/
bool swSpiDevice::writeTo(uint8_t address, uint8_t* writeBuf, size_t numBytes)
{
    for (int i=0; i<sizeof(uint8_t); i++)
    {
        transferByte(writeBuf[i]);
    }
    return true;
}

} // Namespace isochron