/**
* @file A7106.hpp
* @brief Defines the interface to the A7106 device. 
* @note Derived from A7105.
* 
*/

#ifndef A7106_HPP
#define A7106_HPP

#include "swSpiDevice.hpp"
#include "A7105.hpp"

namespace isochron
{

class A7106: public isochron::A7105

{
public:
    A7106(swSpiBus& bus): A7105(bus) {};

}; //A7106

} //namespace isochron

#endif //ifndef