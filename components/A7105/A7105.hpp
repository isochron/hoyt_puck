/**
@file: A7105.hpp

@brief: Software interface for the A7105 RF transceiver.
*/

#include "swSpiDevice.hpp"
#include "DigitalIn.h"

namespace isochron
{

class A7105
{
public:

    A7105(swSpiBus& bus);

    //bool updateStatus();

protected:

    bool m_isBusy {false};

    swSpiDevice m_spiDev;

    //mbed::DigitalIn m_TMOE;

    //mbed::DigitalIn m_WTR;

    //mbed::DigitalIn m_statusPin;

    void configure();

};

} // namespace isochron