/**
@file: A7105.cpp
*/

#include "A7105.hpp"
#include "mbed.h"

#include <cstdint>

namespace isochron
{

/*Config register addresses*/
constexpr uint8_t REG_MODE {0x00};
constexpr uint8_t REG_MODE_CTRL {0x01};
constexpr uint8_t REG_FIFO_I {0x03};
constexpr uint8_t REG_FIFO_II {0x04};
constexpr uint8_t REG_OSC_I {0x07};
constexpr uint8_t REG_OSC_II {0x08};
constexpr uint8_t REG_OSC_III {0x09};
constexpr uint8_t REG_CK0_PIN {0x0A};
constexpr uint8_t REG_GPIO1_PINI {0x0B};
constexpr uint8_t REG_GPIO2_PINII {0x0C};
constexpr uint8_t REG_CLK {0x0D};
constexpr uint8_t REG_DATA_RATE {0x0E};
constexpr uint8_t REG_PLL_I {0x0F};
constexpr uint8_t REG_PLL_II {0x10};
constexpr uint8_t REG_PLL_III {0x11};
constexpr uint8_t REG_PLL_IV {0x12};
constexpr uint8_t REG_PLL_V {0x13};
constexpr uint8_t REG_TX_I {0x14};
constexpr uint8_t REG_TX_II {0x15};
constexpr uint8_t REG_DELAY_I {0x16};
constexpr uint8_t REG_DELAY_II {0x17};
constexpr uint8_t REG_RX {0x18};
constexpr uint8_t REG_RX_GAIN_I {0x19};
constexpr uint8_t REG_RX_GAIN_II {0x1A};
constexpr uint8_t REG_RX_GAIN_III {0x1B};
constexpr uint8_t REG_RX_GAIN_IV {0x1C};
constexpr uint8_t REG_RSSI_THRESH {0x1D};
constexpr uint8_t REG_ADC {0x1E};
constexpr uint8_t REG_CODE_I {0x1F};
constexpr uint8_t REG_CODE_II {0x20};
constexpr uint8_t REG_CODE_III {0x21};
constexpr uint8_t REG_IF_CAL_I {0x22};
constexpr uint8_t REG_VCO_I_CAL {0x24};
constexpr uint8_t REG_VCO_SBCAL_I {0x25}; //VCO single-band calibration
constexpr uint8_t REG_VCO_SBCAL_II {0x26};
constexpr uint8_t REG_BATT_DETECT {0x27};
constexpr uint8_t REG_TX_TEST {0x28};
constexpr uint8_t REG_RX_DEM_I {0x29}; //Rx DEM Test
constexpr uint8_t REG_RX_DEM_II {0x2A};
constexpr uint8_t REG_CPC {0x2B};
constexpr uint8_t REG_CRYS_TEST {0x2C};
constexpr uint8_t REG_PLL_TEST {0x2D};
constexpr uint8_t REG_VCO_TEST_I {0x2E};
constexpr uint8_t REG_VCO_TEST_II {0x2F};
constexpr uint8_t REG_IFAT {0x30};
constexpr uint8_t REG_RSCALE {0x31};


/*Config register values*/
constexpr uint8_t CFG_MODE {0x00};
constexpr uint8_t CFG_MODE_CTRL {0x42};
constexpr uint8_t CFG_FIFO_I {0x25};
constexpr uint8_t CFG_FIFO_II {0x00};
constexpr uint8_t CFG_OSC_I {0x00};
constexpr uint8_t CFG_OSC_II {0x00};
constexpr uint8_t CFG_OSC_III {0x00};
constexpr uint8_t CFG_CK0_PIN {0x00};
constexpr uint8_t CFG_GPIO1_PINI {0x19};
constexpr uint8_t CFG_GPIO2_PINII {0x01};
constexpr uint8_t CFG_CLK {0x05};
constexpr uint8_t CFG_DATA_RATE {0x00};
constexpr uint8_t CFG_PLL_I {0x50};
constexpr uint8_t CFG_PLL_II {0x9E};
constexpr uint8_t CFG_PLL_III {0x4B};
constexpr uint8_t CFG_PLL_IV {0x00};
constexpr uint8_t CFG_PLL_V {0x02};
constexpr uint8_t CFG_TX_I {0x16};
constexpr uint8_t CFG_TX_II {0x2B};
constexpr uint8_t CFG_DELAY_I {0x12};
constexpr uint8_t CFG_DELAY_II {0x40};
constexpr uint8_t CFG_RX {0x62};
constexpr uint8_t CFG_RX_GAIN_I {0x80};
constexpr uint8_t CFG_RX_GAIN_II {0x80};
constexpr uint8_t CFG_RX_GAIN_III {0x00};
constexpr uint8_t CFG_RX_GAIN_IV {0x0A};
constexpr uint8_t CFG_RSSI_THRESH {0x32};
constexpr uint8_t CFG_ADC {0x03};
constexpr uint8_t CFG_CODE_I {0x1F};
constexpr uint8_t CFG_CODE_II {0x1E};
constexpr uint8_t CFG_CODE_III {0x00};
constexpr uint8_t CFG_IF_CAL_I {0x00};
constexpr uint8_t CFG_VCO_I_CAL {0x00};
constexpr uint8_t CFG_VCO_SBCAL_I {0x00};
constexpr uint8_t CFG_VCO_SBCAL_II {0x3B};
constexpr uint8_t CFG_BATT_DETECT {0x00};
constexpr uint8_t CFG_TX_TEST {0x17};
constexpr uint8_t CFG_RX_DEM_I {0x47};
constexpr uint8_t CFG_RX_DEM_II {0x80};
constexpr uint8_t CFG_CPC {0x03};
constexpr uint8_t CFG_CRYS_TEST {0x01};
constexpr uint8_t CFG_PLL_TEST {0x45};
constexpr uint8_t CFG_VCO_TEST_I {0x18};
constexpr uint8_t CFG_VCO_TEST_II {0x99};
constexpr uint8_t CFG_IFAT {0x01};
constexpr uint8_t CFG_RSCALE {0x0F};



constexpr uint8_t TXRX_ADDRESS{0x00};


A7105::A7105(swSpiBus& bus): m_spiDev{swSpiDevice(bus, TXRX_ADDRESS)}
{
    ;
}

void A7105::configure()
{
    //God help us.

    uint8_t registerAddrs[] = {
    REG_MODE, REG_MODE_CTRL, REG_FIFO_I, REG_FIFO_II, REG_OSC_I, REG_OSC_II, REG_OSC_III, 
    REG_CK0_PIN, REG_GPIO1_PINI, REG_GPIO2_PINII, REG_CLK, REG_DATA_RATE, REG_PLL_I,
    REG_PLL_II, REG_PLL_III, REG_PLL_IV, REG_PLL_V, REG_TX_I, REG_TX_II, REG_DELAY_I, 
    REG_DELAY_II, REG_RX, REG_RX_GAIN_I, REG_RX_GAIN_II, REG_RX_GAIN_III, REG_RX_GAIN_IV,
    REG_RSSI_THRESH, REG_ADC, REG_CODE_I, REG_CODE_II, REG_CODE_III, REG_IF_CAL_I, REG_VCO_I_CAL,
    REG_VCO_SBCAL_I, REG_VCO_SBCAL_II, REG_BATT_DETECT, REG_TX_TEST, REG_RX_DEM_I, REG_RX_DEM_II,
    REG_CPC, REG_CRYS_TEST, REG_PLL_TEST, REG_VCO_TEST_I, REG_VCO_TEST_II, REG_IFAT, REG_RSCALE
    };

    uint8_t configValues[] = {
    CFG_MODE, CFG_MODE_CTRL, CFG_FIFO_I, CFG_FIFO_II, CFG_OSC_I, CFG_OSC_II, CFG_OSC_III, 
    CFG_CK0_PIN, CFG_GPIO1_PINI, CFG_GPIO2_PINII, CFG_CLK, CFG_DATA_RATE, CFG_PLL_I,
    CFG_PLL_II, CFG_PLL_III, CFG_PLL_IV, CFG_PLL_V, CFG_TX_I, CFG_TX_II, CFG_DELAY_I, 
    CFG_DELAY_II, CFG_RX, CFG_RX_GAIN_I, CFG_RX_GAIN_II, CFG_RX_GAIN_III, CFG_RX_GAIN_IV,
    CFG_RSSI_THRESH, CFG_ADC, CFG_CODE_I, CFG_CODE_II, CFG_CODE_III, CFG_IF_CAL_I, CFG_VCO_I_CAL,
    CFG_VCO_SBCAL_I, CFG_VCO_SBCAL_II, CFG_BATT_DETECT, CFG_TX_TEST, CFG_RX_DEM_I, CFG_RX_DEM_II,
    CFG_CPC, CFG_CRYS_TEST, CFG_PLL_TEST, CFG_VCO_TEST_I, CFG_VCO_TEST_II, CFG_IFAT, CFG_RSCALE
    };

    for (int i =0; i< sizeof(registerAddrs); i++)
    {
        m_spiDev.writeTo(registerAddrs[i], &configValues[i], 1);
    }
    
    //TODO:
    //log("Done with configuration");
}

/*
bool A7105::updateStatus()
{
    m_isBusy = (m_statusPin.read() | m_TMOE.read() | m_WTR.read());
    return m_isBusy;
}
*/

} //namespace isochron